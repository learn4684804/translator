from typing import Any

from shared.domain import PublicModel

__all__ = ["Response", "ResponseMulti"]


class Response(PublicModel):
    result: Any


class ResponseMulti(PublicModel):
    results: list[Any]
