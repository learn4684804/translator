import os

from config import settings

__all__ = ["import_schemas"]


def import_schemas():
    apps_dir = settings.apps_dir

    for app in os.listdir(apps_dir):
        if app not in settings.apps:
            continue

        __import__(f"apps.{app}.db.schemas")
