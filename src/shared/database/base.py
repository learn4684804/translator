from datetime import datetime

from sqlalchemy import Column, DateTime, Integer
from sqlalchemy.orm import declarative_base

from shared.database.engine import engine

__all__ = ["Base"]


class _Base:
    """Base class for all database models."""

    id = Column(Integer(), primary_key=True)
    created_at = Column(DateTime(), default=datetime.now)
    updated_at = Column(
        DateTime(), default=datetime.now, onupdate=datetime.now
    )


Base = declarative_base(cls=_Base, bind=engine.sync_engine)
