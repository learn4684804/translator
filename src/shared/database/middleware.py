from fastapi import Request, Response
from starlette.middleware.base import (
    BaseHTTPMiddleware,
    RequestResponseEndpoint,
)

from shared.database import session_manager

__all__ = ["DatabaseTransactionMiddleware"]


class DatabaseTransactionMiddleware(BaseHTTPMiddleware):
    async def dispatch(
        self, request: Request, call_next: RequestResponseEndpoint
    ) -> Response:
        session = session_manager.get_session()
        async with session.begin_nested():
            try:
                result = await call_next(request)
                await session.commit()
                return result
            except Exception:
                await session.rollback()
                raise
            finally:
                await session_manager.close()
