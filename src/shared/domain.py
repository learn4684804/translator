from pydantic import BaseModel, Extra


class InternalModel(BaseModel):
    class Config:
        extra = Extra.forbid
        orm_mode = True
        allow_population_by_field_name = True
        validate_assignment = True


class PublicModel(BaseModel):
    class Config:
        extra = Extra.ignore
        orm_mode = True
        allow_population_by_field_name = True
        validate_assignment = True
