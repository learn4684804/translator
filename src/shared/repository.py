from typing import TypeVar

from pydantic.schema import Generic
from sqlalchemy.engine import Result
from sqlalchemy.orm import Query

from shared.database import Base, session_manager

ConcreteSchema = TypeVar("ConcreteSchema", bound=Base)

__all__ = ["BaseRepository"]


class BaseRepository(Generic[ConcreteSchema]):
    def __init__(self) -> None:
        self.session = session_manager.get_session()

    async def _execute(self, query: Query) -> Result:
        return await self.session.execute(query)

    async def _create(
        self, schemas: list[ConcreteSchema]
    ) -> list[ConcreteSchema]:
        self.session.add_all(schemas)
        await self.session.flush()
        for schema in schemas:
            await self.session.refresh(schema)
        return schemas
