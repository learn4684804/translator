from pydantic import BaseModel, Field

__all__ = ["QueryParams", "prepare_query_params"]


class QueryParams(BaseModel):
    filters: dict[str, str | int | float] = Field(default_factory=dict)
    sorting: list[str] = Field(default_factory=list)
    search: str | None = None
    page: int = 1
    limit: int = 10


def prepare_query_params(**kwargs) -> QueryParams:
    params = QueryParams()
    for key, value in kwargs.items():  # type:str, str
        if key == "sorting" and value:
            params.sorting = value.split(",")
        elif key == "search" and value:
            params.search = value
        elif key == "page" and value:
            params.page = int(value)
        elif key == "limit" and value:
            params.limit = int(value)
        elif value:
            params.filters[key] = value
    return params
