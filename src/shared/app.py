from fastapi import FastAPI

__all__ = ["create_app"]


def create_app(routers: list, middlewares: list):
    app = FastAPI()

    for router in routers:
        app.include_router(router)

    for middleware in middlewares:
        app.add_middleware(middleware)

    return app
