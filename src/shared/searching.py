from functools import reduce

from sqlalchemy import or_, Unicode

__all__ = ["Searching"]


class Searching(object):
    search_fields = list()

    @classmethod
    def get_clauses(cls, search_term):
        clauses = []
        if not search_term or not cls.search_fields:
            return None
        for search_field in cls.search_fields:
            clauses.append(
                search_field.cast(Unicode()).ilike(f"%{search_term}%")
            )
        return reduce(or_, clauses)
