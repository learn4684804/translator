from sqlalchemy import Column, String
from sqlalchemy.dialects.postgresql import JSONB

from shared.database import Base

__all__ = ["TranslationSchema"]


class TranslationSchema(Base):
    __tablename__ = "translations"

    source_language = Column(String(255))
    to_language = Column(String(255))
    text = Column(String(), index=True)
    translations = Column(JSONB())
    examples = Column(JSONB())
    definitions = Column(JSONB())
