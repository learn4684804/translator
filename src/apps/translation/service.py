__all__ = ["TranslationService"]

from bs4 import BeautifulSoup
from pyppeteer import launch
from pyppeteer.browser import Browser
from pyppeteer.page import Page

from apps.translation.db.schemas import TranslationSchema
from apps.translation.domain import Translation, Definition, PureTranslation
from apps.translation.repository import TranslationRepo
from config import settings
from shared.query_param import QueryParams


class GoogleTranslateService:
    url = "https://translate.google.com/?sl={0}&tl={1}&text={2}&op=translate"
    default_options = {
        "headless": True,
        "executablePath": settings.pyppeteer.exec_path,
        "defaultViewport": dict(width=1500, height=900),
        "handleSIGINT": False,
        "handleSIGTERM": False,
        "handleSIGHUP": False,
    }
    delay = 10
    waiting_selector = "//body/c-wiz/div/div[2]/c-wiz/div[2]/c-wiz/div[2]/c-wiz/div/div/div[2]/div[1]/div/div[1]/h3"

    def __init__(self, src_language, to_language, text):
        self.browser: Browser | None = None
        self.src_language = src_language
        self.to_language = to_language
        self.text = text
        self.html = ""
        self.soup: BeautifulSoup | None = None
        self.page: Page | None = None

    async def get(self):
        await self._lunch()
        self.page = await self.browser.newPage()
        await self.goto(
            self.url.format(self.src_language, self.to_language, self.text)
        )
        await self.wait_for_element(self.waiting_selector)
        self.html = await self.page.content()
        self.soup = BeautifulSoup(self.html, features="html.parser")
        await self._stop()

    async def _lunch(self):
        self.browser = await launch(
            self.default_options, args=["--no-sandbox"]
        )

    async def _stop(self):
        await self.browser.close()

    @property
    def translations(self) -> list[str]:
        assert self.soup is not None, Exception(
            "First get data using `get` method"
        )

        translations = set()
        elements = self.soup.findAll(
            "span", {"data-sl": self.to_language, "data-tl": self.src_language}
        )
        for element in elements:
            translations.add(element.text)

        return list(translations)

    @property
    def definitions(self) -> list[Definition]:
        assert self.soup is not None, Exception(
            "First get data using `get` method"
        )

        definitions = dict()
        elements = self.soup.findAll(
            "div",
            {
                "jsname": None,
                "data-sl": None,
                "lang": self.src_language,
                "role": None,
                "tabindex": None,
                "data-tl": None,
            },
        )
        for element in elements:
            if element.next_element.name is not None:
                continue
            elif len(element.contents) != 1:
                continue
            definition_text = element.text

            definition = definitions.get(definition_text, Definition())

            definition.definition = definition_text
            definitions[definition_text] = definition

        self._get_synonyms(definitions)

        return list(definitions.values())

    def _get_synonyms(self, definitions: dict):
        elements = self.soup.findAll(
            "span",
            {
                "data-sl": self.src_language,
                "lang": self.src_language,
                "role": "button",
                "tabindex": 0,
                "data-tl": None,
            },
        )
        for element in elements:
            definition_text = element.parent.parent.next_element.text
            definition = definitions.get(definition_text, Definition())
            definition.synonyms.append(element.text)

    @property
    def examples(self) -> list[str]:
        assert self.soup is not None, Exception(
            "First get data using `get` method"
        )

        examples = set()
        elements = self.soup.findAll("b")
        for element in elements:
            examples.add(element.parent.text)
        return list(examples)

    async def goto(self, url):
        await self.page.goto(url, {"waitUntil": "domcontentloaded"})

    async def wait_for_element(self, selector):
        await self.page.waitForXPath(selector, timeout=self.delay * 1000)


class TranslationService:
    async def get_from_db(
            self, source_language: str, to_language: str, text: str
    ) -> Translation | None:
        result = await TranslationRepo().get_translation(
            source_language, to_language, text
        )
        if not result:
            return None
        return Translation.from_orm(result)

    async def get_from_google(
            self, source_language: str, to_language: str, text: str
    ) -> Translation:
        translate_service = GoogleTranslateService(
            source_language, to_language, text
        )
        await translate_service.get()
        return Translation(
            text=text,
            translations=translate_service.translations,
            source_language=source_language,
            to_language=to_language,
            definitions=translate_service.definitions,
            examples=translate_service.examples,
        )

    async def save_to_db(self, translation: Translation) -> Translation:
        result = await TranslationRepo().create(
            TranslationSchema(
                source_language=translation.source_language,
                to_language=translation.to_language,
                text=translation.text,
                translations=translation.translations,
                examples=translation.examples,
                definitions=[
                    definition.dict() for definition in translation.definitions
                ],
            )
        )
        return Translation.from_orm(result)

    async def get_list(
            self, query_params: QueryParams
    ) -> list[PureTranslation]:
        schemas = await TranslationRepo().get_list(
            query_params.filters,
            query_params.sorting,
            query_params.search,
            query_params.page,
            query_params.limit,
        )
        translations = []
        for schema in schemas:
            if query_params.filters.get("is_detailed"):
                translations.append(Translation.from_orm(schema))
            else:
                translations.append(PureTranslation.from_orm(schema))

        return translations

    async def delete(self, id_: int):
        await TranslationRepo().delete_by_id(id_)
