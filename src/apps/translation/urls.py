from fastapi import APIRouter

from apps.translation.api import translate, list_translations, delete_translation

router = APIRouter(prefix="/translation")

router.post("/", status_code=200)(translate)
router.get("/", status_code=200)(list_translations)
router.delete("/{id_}/", status_code=204)(delete_translation)
