from sqlalchemy import select, delete
from sqlalchemy.orm import Query

from apps.translation.db.schemas import TranslationSchema
from shared.filtering import Filtering, FilterField
from shared.repository import BaseRepository

__all__ = ["TranslationRepo"]

from shared.searching import Searching
from shared.sorting import Sorting


class TranslationFilter(Filtering):
    source_language = FilterField(TranslationSchema.source_language)
    to_language = FilterField(TranslationSchema.to_language)


class TranslationSearch(Searching):
    search_fields = [TranslationSchema.text]


class TranslationSorting(Sorting):
    id = TranslationSchema.id
    text = TranslationSchema.text


class TranslationRepo(BaseRepository):
    async def get_translation(
            self, src_language: str, to_language: str, text: str
    ) -> TranslationSchema | None:
        query: Query = select(TranslationSchema)
        query = query.where(TranslationSchema.source_language == src_language)
        query = query.where(TranslationSchema.to_language == to_language)
        query = query.where(TranslationSchema.text == text)

        db_result = await self._execute(query)
        results = db_result.scalars().all()
        if len(results):
            return results[0]
        return

    async def create(
            self, translation: TranslationSchema
    ) -> TranslationSchema:
        [translation] = await self._create([translation])
        return translation

    async def get_list(
            self,
            filters: dict | None = None,
            sorting: list[str] | None = None,
            search: None = None,
            page=1,
            limit=10,
    ) -> list[TranslationSchema]:
        if page < 1:
            page = 1
        if sorting is None:
            sorting = ["-id"]

        query: Query = select(TranslationSchema)
        if filters:
            query = query.where(*TranslationFilter().get_clauses(**filters))
        if sorting:
            query = query.order_by(*TranslationSorting().sort_by(*sorting))
        if search:
            query = query.where(TranslationSearch.get_clauses(search))
        query = query.offset(page - 1)
        query = query.limit(limit)

        db_result = await self._execute(query)
        return db_result.scalars().all()

    async def delete_by_id(self, id_: int):
        query: Query = delete(TranslationSchema)
        query = query.where(TranslationSchema.id == id_)
        db_result = await self._execute(query)
        print(db_result)
