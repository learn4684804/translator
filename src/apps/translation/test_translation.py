import json

import pytest
from sqlalchemy import text, Integer

from apps.translation.db.schemas import TranslationSchema
from apps.translation.repository import TranslationRepo
from shared.database import session_manager, Base

list_url = "/translation/"
detail_url = "/translation/{}/"


@pytest.fixture(autouse=True)
async def truncate_tables():
    """
    For each test it will clear database
    :return:
    """
    session = session_manager.get_session()
    for name, table in Base.metadata.tables.items():
        query = text(f"""TRUNCATE "{name}" CASCADE""")
        await session.execute(query)
    await session.commit()


@pytest.fixture(autouse=True)
async def update_sequence():
    """
    For each test it updates sequences
    """
    session = session_manager.get_session()
    for name, table in Base.metadata.tables.items():
        for primary_key in table.primary_key.columns:
            if not isinstance(primary_key.type, Integer):
                continue
            if primary_key.autoincrement not in ("auto", True):
                continue
            query = text(
                f"""
            SELECT SETVAL('{name}_{primary_key.name}_seq',
                (SELECT COALESCE(MAX(id), 0) FROM "{name}") + 1, false)
            """
            )
            await session.execute(query)
    await session.commit()


@pytest.fixture()
async def translations():
    await TranslationRepo().create(TranslationSchema(
        source_language='en',
        to_language='ru',
        text='challenge',
        translations=['вызов'],
        examples=['a world title challenge'],
        definitions=[
            dict(
                definition='a call to take part in a contest or competition, especially a duel.',
                synonyms=['dare']
            )]
        ,
    ))

    await TranslationRepo().create(TranslationSchema(
        source_language='en',
        to_language='uz',
        text='challenge',
        translations=['chaqirish'],
        examples=['a world title challenge'],
        definitions=[
            dict(
                definition='a call to take part in a contest or competition, especially a duel.',
                synonyms=['dare']
            )]
        ,
    ))

    await TranslationRepo().create(TranslationSchema(
        source_language='en',
        to_language='ru',
        text='home',
        translations=['дома'],
        examples=['they have made Provence their home'],
        definitions=[
            dict(
                definition='the place where one lives permanently, especially as a member of a family or household.',
                synonyms=['property']
            )]
        ,
    ))


async def test_translate_challenge(client):
    data = dict(source_language="en", to_language="ru", text="challenge")
    response = await client.post(list_url, content=json.dumps(data))
    assert response.status_code == 200
    body = response.json()

    assert len(body["result"]["translations"]) == len(
        [
            "давать отвод присяжным",
            "опознавательные сигналы",
            "вызов",
            "спрашивать пароль",
            "бросать вызов",
            "требовать",
            "вызов на дуэль",
            "сомневаться",
            "спрашивать пропуск",
            "отвод",
            "сомнение",
            "оспаривать",
            "сложная задача",
            "отрицать",
            "вызывать",
            "подвергать сомнению",
            "оклик",
            "окликать",
            "проблема",
        ]
    )
    assert len(body["result"]["definitions"]) == 6
    assert len(body["result"]["examples"]) == 7


async def test_list(client, translations):
    response = await client.get(list_url)
    assert response.status_code == 200
    assert len(response.json()['results']) == 3


async def test_list_filter(client, translations):
    response = await client.get(f'{list_url}?to_language=uz')
    assert response.status_code == 200
    assert len(response.json()['results']) == 1


async def test_list_search(client, translations):
    response = await client.get(f'{list_url}?search=home')
    assert response.status_code == 200
    assert len(response.json()['results']) == 1


async def test_list_sorting(client, translations):
    response = await client.get(f'{list_url}?sorting=-text')
    assert response.status_code == 200
    assert len(response.json()['results']) == 3
    assert response.json()['results'][0]['text'] == 'home'

    response = await client.get(f'{list_url}?sorting=text')
    assert response.status_code == 200
    assert len(response.json()['results']) == 3
    assert response.json()['results'][-1]['text'] == 'home'


async def test_list_page_limit(client, translations):
    response = await client.get(f'{list_url}?page=1&limit=1')
    assert response.status_code == 200
    assert len(response.json()['results']) == 1
    assert response.json()['results'][0]['text'] == 'challenge'
    assert response.json()['results'][0]['source_language'] == 'en'
    assert response.json()['results'][0]['to_language'] == 'ru'

    response = await client.get(f'{list_url}?page=2&limit=1')
    assert response.status_code == 200
    assert len(response.json()['results']) == 1
    assert response.json()['results'][0]['text'] == 'challenge'
    assert response.json()['results'][0]['source_language'] == 'en'
    assert response.json()['results'][0]['to_language'] == 'uz'


async def test_delete_translation(client, translations):
    response = await client.delete(detail_url.format(1))
    assert response.status_code == 204

    response = await client.get(list_url)
    assert response.status_code == 200
    assert len(response.json()['results']) == 2
