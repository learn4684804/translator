from fastapi import Body

from apps.translation.domain import (
    TranslateRequest,
    TranslationPublic,
    DefinitionPublic,
)
from apps.translation.service import TranslationService

__all__ = ["translate", "list_translations", 'delete_translation']

from shared.query_param import prepare_query_params

from shared.response import Response, ResponseMulti


async def translate(
        translate_request: TranslateRequest = Body(...),
) -> Response:
    translation_service = TranslationService()
    translation = await translation_service.get_from_db(
        translate_request.source_language,
        translate_request.to_language,
        translate_request.text,
    )
    if not translation:
        draft_translation = await translation_service.get_from_google(
            translate_request.source_language,
            translate_request.to_language,
            translate_request.text,
        )
        translation = await translation_service.save_to_db(draft_translation)
    return Response(
        result=TranslationPublic(
            id=translation.id,
            translations=translation.translations,
            definitions=[
                DefinitionPublic(
                    definition=definition.definition,
                    synonyms=definition.synonyms,
                )
                for definition in translation.definitions
            ],
            examples=translation.examples,
        )
    )


async def list_translations(
        source_language: str | None = None,
        to_language: str | None = None,
        search: str | None = None,
        sorting: str | None = None,
        page: int = 1,
        limit: int = 10,
        is_detailed: bool = False,
) -> ResponseMulti:
    query_params = prepare_query_params(
        source_language=source_language,
        to_language=to_language,
        search=search,
        sorting=sorting,
        page=page,
        limit=limit,
        is_detailed=is_detailed,
    )
    translations = await TranslationService().get_list(query_params)
    return ResponseMulti(results=translations)


async def delete_translation(id_: int):
    await TranslationService().delete(id_)
