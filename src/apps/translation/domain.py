from pydantic import Field

from shared.domain import InternalModel, PublicModel

__all__ = [
    "TranslateRequest",
    "PureTranslation",
    "Translation",
    "TranslationPublic",
    "Definition",
    "DefinitionPublic",
]


class Definition(InternalModel):
    definition: str = ""
    synonyms: list[str] = Field(default_factory=list)


class DefinitionPublic(PublicModel):
    definition: str
    synonyms: list[str]


class TranslateRequest(InternalModel):
    text: str
    source_language: str
    to_language: str


class PureTranslation(InternalModel):
    id: int | None
    source_language: str
    to_language: str
    text: str


class Translation(PureTranslation):
    translations: list[str]
    definitions: list[Definition]
    examples: list[str]


class TranslationPublic(PublicModel):
    id: int
    translations: list[str]
    definitions: list[DefinitionPublic]
    examples: list[str]
