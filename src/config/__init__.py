from pathlib import Path

from pydantic import BaseSettings

from config.database import PostgresDatabase
from config.pyppeteer import Pyppeteer


class Settings(BaseSettings):
    env: str = "dev"

    root_dir: Path
    apps_dir: Path

    database: PostgresDatabase = PostgresDatabase()
    pyppeteer: Pyppeteer = Pyppeteer()
    apps: list[str]

    class Config:
        env_nested_delimiter = "__"
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings(
    root_dir=Path(__file__).parent.parent,
    apps_dir=Path(__file__).parent.parent / "apps",
    apps=[
        "translation",
    ],
)
