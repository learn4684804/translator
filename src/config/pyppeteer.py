from pydantic import BaseModel

__all__ = ["Pyppeteer"]


class Pyppeteer(BaseModel):
    exec_path: str | None = None
