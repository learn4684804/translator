import json
from typing import Any

from pydantic import BaseModel

__all__ = ["PostgresDatabase"]


class PostgresDatabase(BaseModel):
    host: str = "localhost"
    port: str = "5432"
    password: str = "postgres"
    user: str = "postgres"
    db: str = "postgres"
    options: dict[str, Any] = dict(
        json_serializer=lambda x: json.dumps(x),
        json_deserializer=lambda x: json.loads(x),
    )

    @property
    def url(self) -> str:
        return (
            f"postgresql+asyncpg://{self.user}:{self.password}"
            f"@{self.host}:{self.port}/{self.db}"
        )
