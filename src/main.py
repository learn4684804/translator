from registry import middlewares, routers
from shared.app import create_app

__all__ = ["app"]

app = create_app(routers, middlewares)
