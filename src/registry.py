from apps.translation.urls import router

__all__ = ["routers", "middlewares"]

from shared.database.middleware import DatabaseTransactionMiddleware

routers = [router]
middlewares = [DatabaseTransactionMiddleware]
