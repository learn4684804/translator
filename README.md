# Translator

## <span style="color:#9DB7FF">About</span>
👉 This is a micro-service to translate using google and store the result into database 

🔌 **Web application is powered by:**
- ✅ [Python3.10+](https://www.python.org/downloads/release/python-3108/)
- ✅ [FastAPI](https://fastapi.tiangolo.com)
- ✅ [Postgesql](https://www.postgresql.org/docs/14/index.html)
- ✅ [Docker](https://docs.docker.com/get-docker/)
- ✅ [Pydantic](https://pydantic-docs.helpmanual.io)
- ✅ [SQLAlchemy](https://www.sqlalchemy.org/)
## ✋ <span style="color:#9DB7FF">Mandatory steps</span>

### 1. Clone the project 🌐

```bash
git clone https://gitlab.com/learn4684804/translator.git
```

### 2. Setup environment variables ⚙️

👉 All default variables configured for making easy to run application via Docker in a few clicks

> 💡 All of them you can find in `.env.default`


#### 2.1 Description 📜
| Key | Default value                                                  | Description                                                                                                                                                                   |
| --- |----------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| PYTHONPATH | src/ | This variable makes all folders inside `src/` reachable in a runtime. </br> ***NOTE:*** You don't need to do this if you use Docker as far as it is hardcoded in `Dockerfile` |
| DATABASE__HOST | postgres | Database Host |
| DATABASE__USER | postgres | User name for Postgresql Database user |
| DATABASE__PASSWORD | postgres | Password for Postgresql Database user |
| DATABASE__DB | postgres | Database name |

##### ✋ Mandatory:

> You can see that some environment variables have double underscore (`__`) instead of `_`.
>
> As far as `pydantic` supports [nested settings models](https://pydantic-docs.helpmanual.io/usage/settings/) it uses to have cleaner code

#### 2.2 Create `.env` file for future needs

It is hightly recommended to create `.env` file as far as it is needed for setting up the project with Local and Docker approaches.

```bash
cp .env.default .env
```

### 2. Install all project dependencies 🧱

Pipenv used as a default dependencies manager

```bash
# Create virtual environment and activate 
mkvirtualenv .venv -p python3.10+
source .venv/bin/activate

# Install requirements
pip install -r requirements.txt
```
<br>
Export environment variables using a Bash-script

```bash
set -o allexport; source .env; set +o allexport
```

### 3. Running the application ▶️
```bash
uvicorn src.main:app --proxy-headers --port {PORT} --reload
```

### 4. Running Tests ▶️

The `pytest` framework is using in order to write unit tests.

```bash
# To run tests
pytest
```

## 🐳 <span style="color:#9DB7FF">Docker development</span>

### 1. Build application images 🔨

```bash
docker-compose build
```
✅ Make sure that you completed `.env` file. It is using by default in `docker-compose.yaml` file for buildnig.

✅ Check building with `docker images` command. You should see the record with `fastapi_service`.

### 2. Running the application ▶️

```bash
docker-compose up
```

#### Stop the application 🛑
```bash
docker-compose down
```