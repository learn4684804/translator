# M1 chip support with python3.10.8
FROM --platform=linux/arm64 python:3.10.8
ENV PYTHONUNBUFFERED 1
ENV PROJECT_NAME app
ENV PROJECT_PATH /$PROJECT_NAME

# set origin path
ENV PYTHONPATH="src/"

# create path for project
RUN mkdir -p $PROJECT_PATH

# install needed libs + chromium for pyppeteer
RUN apt-get update && apt-get install -y postgresql g++ libffi-dev chromium --no-install-recommends

# move requirements to docker environments project path
ADD ./requirements.txt $PROJECT_PATH/

# install project requirements
RUN pip3 install --no-cache-dir -r $PROJECT_PATH/requirements.txt

# copy project into virual environment
COPY . $PROJECT_PATH

# add execution mode to waiting script
RUN chmod +x $PROJECT_PATH/wait-for.sh

# copy starting point
COPY ./docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]