#!/bin/sh
set -e

cd $PROJECT_PATH
echo -e "Project configuration"
echo WORKING_DIRECOTRY:$PROJECT_PATH

echo -e "\nDatabase configuration"
echo DATABASE_HOST:$DATABASE__HOST
echo DATABASE_PORT:$DATABASE__PORT
echo DATABASE_USER:$DATABASE__USER
echo DATABASE_PASSWORD:*************
echo DATABASE_NAME:$DATABASE__NAME

./wait-for.sh $DATABASE__HOST:$DATABASE__PORT -t 100

alembic upgrade head

echo "\nRun app\n"
uvicorn src.main:app --reload --host 0.0.0.0 --port 80 --proxy-headers
